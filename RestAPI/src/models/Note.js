const { Schema, model } = require('mongoose');

const noteSchema = new Schema({
    title: {
        type: String,
        required: true,
        min: 2
    },
    text: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: false
    }
});

module.exports = model('Note', noteSchema);
