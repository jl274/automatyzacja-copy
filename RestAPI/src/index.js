// Imports
const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const notes = require('./routes/notes');

// Middle-wares
app.use(express.json());
app.use(cors())

//DB config
const dbConnData = {
    host: process.env.MONGO_HOST || '127.0.0.1',
    port: process.env.MONGO_PORT || 27017,
    database: process.env.MONGO_DATABASE || 'local'
};


// endpoints
app.use('/notes', notes);

// listening  
const port = process.env.PORT || 420
    app.listen(port, () => {
      console.log(`API server listening at http://localhost:${port}`);
    });

mongoose
  .connect(`mongodb://${dbConnData.host}:${dbConnData.port}/${dbConnData.database}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(response => {
    console.log(`Connected to MongoDB. Database name: "${response.connections[0].name}"`)
    
  })
  .catch(error => console.error('Error connecting to MongoDB', error));
