// imports
const express = require('express');
const router = express.Router();

const Note = require('../models/Note');

// helper functions
const checkForMissingArguments = (res, ...args) => {
    for (const property of args) {
        if (property == null) {
            return res.status(400).json({error: "Missing arguments"});
        }
    }
}

// routes
router.get('/', async (req, res) => {
    try {

        const notes = await Note.find({});
        return res.json(notes);

    } catch (err) {
        return res.status(500).json({err});
    }
});

router.post('/', async (req, res) => {
    try {

        const {title, text, author} = req.body;
        checkForMissingArguments(res, title, text, author);
        
        const newNote = await Note.create({title, text, author});

        return res.json(newNote);

    } catch (err) {
        return res.status(500).json({err});
    }
});

router.delete('/:id', async (req, res) => {
    try {

        const { id } = req.params;
        checkForMissingArguments(res, id);

        const exists = await Note.exists({_id: id});
        if (!exists) {
            return res.status(404).json({err: "Note not found"});
        }

        await Note.deleteOne({_id: id});
        return res.json({id});

    } catch (err) {
        return res.status(500).json({err});
    }
});

router.put('/:id', async (req, res) => {
    try {

        const { id } = req.params;
        const {title, text, author} = req.body;
        checkForMissingArguments(res, id, title, text, author);

        const exists = await Note.exists({_id: id});
        if (!exists) {
            return res.status(404).json({err: "Note not found"});
        }

        await Note.updateOne({_id: id}, {title, text, author});
        const editedNote = await Note.findOne({_id: id});
        return res.json(editedNote);

    } catch (err) {
        return res.status(500).json({err});
    }
});


module.exports = router;